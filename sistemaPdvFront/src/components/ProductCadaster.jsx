import { Header } from "./Header";
import styles from "../styles/ProductCadaster.module.css"
export function ProductCadaster(){
    return(
        <>
            <Header/>
            <h2 className='pageTitle'>Cadastro Produto</h2>
            <section className={styles.formCadasterProduct}>
                <div className={styles.containerProductCadaster}>
                    <label htmlFor="">Nome</label>
                    <input type="text" />
                    <label htmlFor="">Preço de Compra</label>
                    <input type="text" placeholder="R$000,00"/>
                    <label htmlFor="">Preço de Venda</label>
                    <input type="text" placeholder="R$000,00"/>
                    <div className={styles.buttonsProductCadaster}>
                        <button className={styles.cadasterButton}>Cadastrar</button>
                        <button className={styles.cancelButton}>Cancelar</button>
                    </div>
                </div>

            </section>
            
        </>
    )
}