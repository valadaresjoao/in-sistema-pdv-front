import styles from '../styles/productItem.module.css';
import { PlusCircle } from "phosphor-react";
export function ProductItem(){
    return(
        <div className={styles.productContainer}>
            <div className={styles.productInfo}>
                <div className={styles.nameRow}>
                    <h3 className={styles.name}>Nome:</h3>
                    <h3 className={styles.productName}>Nome Exemplo</h3>
                </div>
                <div className={styles.infoRow}>
                    <h4 className={styles.price}>Preço:</h4>
                    <h4 className={styles.productPrice}> 00</h4>
                    <h4 className={styles.quantity}>Quantidade: </h4>
                    <h4 className={styles.productQuantity}>00 </h4>
                </div>
            </div>
            <hr className={styles.verticalLine}/>
            <div className={styles.quantitySection}>
                <h4 className={styles.quantityTitle}>Quantidade</h4>
                <div className={styles.modularQuantity}>
                    <input className={styles.amount} placeholder='000' disabled></input>
                    <div className={styles.modularButtons}>
                        <button className={styles.buttonAdd}>+</button>
                        <button className={styles.buttonRemove}>-</button>
                    </div>
                </div>
            </div>
            <button className={styles.buttonCart}>
                <PlusCircle size={24} />
                <h2>Carrinho</h2>
            </button>
        </div>
    )
}