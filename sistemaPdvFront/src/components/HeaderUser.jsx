import styles from '../styles/header.module.css'
export function HeaderUser(){
    return(
        <header>
            <div className={styles.mainDivHeader}>
                <div>
                    <h1>Sistema PDV</h1>
                </div>
                <div className={styles.itemsRightHeader}>          
                    <h4>Nome de Usuário</h4>
                    <button>Sair</button>
                </div>
            </div>
        </header>        
    )
}